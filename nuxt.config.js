export default {
  /*
   ** Nuxt target
   ** See https://nuxtjs.org/api/configuration-target
   */
  target: "static",
  /*
   ** Headers of the page
   ** See https://nuxtjs.org/api/configuration-head
   */
  head: {
    htmlAttrs: {
      lang: "fa"
    },
    title: "Learn Aviation | Discover a World of Aviation Training Material",
    meta: [
      {
        charset: "utf-8"
      },
      {
        name: "viewport",
        content: "width=device-width, initial-scale=1"
      },
      {
        hid: "description",
        name: "description",
        content:
          "هدف ما آموزش اصولی زبان هوانوردی و پیشرفت سطح دانش زبان شما با برگزاری کلاس های خصوصی و نمیه خصوصی و ماک تست است."
      },
      {
        hid: "keywords",
        name: "keywords",
        content:
          "aviation, lpr, level4, level 4, pilot, aviation english, cabin crew, flight attendant, student pilot, pilot training, airline, icao, easa, faa, language, iran, cao, online, instructor, crm, آموزش lpr, خلبانی, آزمون هوانوردی, آزمون هوانوردی"
      },

      {
        hid: "twitter:card",
        name: "twitter:card",
        content:
          "Learn Aviation | Discover a World of Aviation Training Material"
      },

      { hid: "twitter:site", name: "twitter:site", content: "@learn_aviation" },

      {
        hid: "twitter:creator",
        name: "twitter:creator",
        content: "@learn_aviation"
      },

      {
        hid: "twitter:title",
        name: "twitter:title",
        content:
          "Learn Aviation | Discover a World of Aviation Training Material"
      },

      {
        hid: "twitter:description",
        name: "twitter:description",
        content:
          "Learn Aviation | Discover a World of Aviation Training Material"
      },

      {
        hid: "twitter:image",
        name: "twitter:image",
        content: "/favicon.ico"
      },

      {
        hid: "description",
        name: "description",
        content:
          "Learn Aviation | Discover a World of Aviation Training Material"
      },

      {
        hid: "og:image",
        property: "og:image",
        content: "/favicon.ico"
      },

      {
        hid: "og:site_name",
        name: "og:site_name",
        content:
          "Learn Aviation | Discover a World of Aviation Training Material"
      },

      { hid: "og:title", name: "og:title", content: "Learn Aviation" },

      {
        hid: "og:description",
        name: "og:description",
        content:
          "Learn Aviation | Discover a World of Aviation Training Material"
      }
    ],

    link: [
      {
        rel: "icon",
        type: "image/x-icon",
        href: "/favicon.ico"
      }
    ]
  },
  /*
   ** Global CSS
   */
  css: ["plyr/dist/plyr.css", "~/assets/css/main.scss"],

  modules: ["bootstrap-vue/nuxt"],

  /*
   ** Plugins to load before mounting the App
   ** https://nuxtjs.org/guide/plugins
   */
  plugins: [{ src: "~/plugins/vue-plyr", mode: "client" }],
  /*
   ** Auto import components
   ** See https://nuxtjs.org/api/configuration-components
   */
  components: true,
  /*
   ** Nuxt.js dev-modules
   */
  buildModules: [
    // Doc: https://github.com/nuxt-community/nuxt-tailwindcss
    "@nuxtjs/tailwindcss",
    [
      "@nuxtjs/google-analytics",
      {
        id: "UA-166472260-1"
      }
    ]
  ],
  /*
   ** Nuxt.js modules
   */
  modules: [
    // Doc: https://github.com/nuxt/content
    "@nuxt/content",
    "bootstrap-vue/nuxt",
    "@nuxtjs/axios",
    "@nuxtjs/pwa",
    "vue-scrollto/nuxt",
    [
      "@nuxtjs/robots",
      {
        robots: {
          UserAgent: "*",
          Disallow: "/"
        }
      }
    ],
    [
      "nuxt-fontawesome",
      {
        imports: [
          {
            set: "@fortawesome/free-solid-svg-icons",
            icons: ["fas"]
          },
          {
            set: "@fortawesome/free-brands-svg-icons",
            icons: ["fab"]
          }
        ]
      }
    ],
    "@nuxtjs/sitemap"
  ],

  sitemap: {
    hostname: "https://learn-aviation.com/",
    path: "/sitemap.xml",
    gzip: true
  },
  /*
   ** Content module configuration
   ** See https://content.nuxtjs.org/configuration
   */
  content: {
    markdown: {
      prism: {
        theme: "prism-themes/themes/prism-material-oceanic.css"
      }
    },
    nestedProperties: ["author.name"]
  },
  /*
   ** Build configuration
   ** See https://nuxtjs.org/api/configuration-build/
   */
  build: {
    manifest: {
      name: "Learn Aviation",
      short_name: "Learn Aviation",
      description: "Discover a World of Aviation Training.",
      lang: "fa",
      start_url: ".",
      display: "standalone",
      background_color: "#fff"
    },

    transition: {
      name: "fade",
      mode: "out-in"
    }
  }
};
